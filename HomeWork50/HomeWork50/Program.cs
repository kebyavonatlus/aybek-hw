﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace HomeWork50
{
    class Program
    {
        static void Main(string[] args)
        {
            var thread1 = new Thread(ForThread1);
            var thread2 = new Thread(ForThread2);
            thread1.Name = "thread1";
            thread2.Name = "thread2";

            thread2.Start();
            thread1.Start();
            PrintTread(thread1);
            PrintTread(thread2);


            //for (int i = 0; i < 100; i++)
            //{
            //    var thr = new Thread(F2);
            //    thr.Start();
            //}



            Console.Read();
        }

        public static void ForThread1()
        {
            for (int i = 1; i <= 10; i++)
            {
                Console.WriteLine($"Thread1: {i}");
                Thread.Sleep(1000);
            }
        }

        public static void ForThread2()
        {
            for (int i = 10; i > 0; i--)
            {
                Console.WriteLine($"Thread1: {i}");
                Thread.Sleep(1000);
            }
        }

        public static void PrintTread(Thread thread)
        {
            Console.WriteLine($"Name: {thread.Name} \nisAlive: {thread.IsAlive} \nPriority: {thread.Priority} \nThreadState: {thread.ThreadState}");
        }

        public static void F1()
        {
            for (int i = 30; i > 0; i--)
            {
                Console.WriteLine($"F1: {i}");
                Thread.Sleep(1000);
            }
        }

        public static void F2()
        {
            for (int i = 30; i > 0; i--)
            {
                Console.WriteLine($"F2: {i}");
                Thread.Sleep(1000);
                for (int j = 0; j < 1000000; j++)
                {

                }
            }
        }
    }
}
